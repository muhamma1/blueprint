export const googleMapKey='AIzaSyCaXARK2H4n1Tl2gIB-0Q8OwvY2I0ICv-A';

 export const listOfCity = [
            { label: "ISLAMABAD" },
            { label: "RAWALPINDI" },
            { label: "GUJRANWALA" },
            { label: "LAHORE" },
            { label: "MULTAN" },
            { label: "PESHAWAR" },
        ];

        export const Cities = [
            { label: "",value:"" },
            { label: "ISLAMABAD",value:"islamabad" },
            { label: "RAWALPINDI",value:"rawalpindi" },
            { label: "GUJRANWALA",value:"gujranwala" },
            { label: "LAHORE",value:"lahore" },
            { label: "MULTAN",value:"multan" },
            { label: "PESHAWAR",value:"peshawar" },
        ];
         export const userRoleType = [
            { label: "DATA ENTRY",value:"dataEntry" },
            { label: "ADMIN",value:"admin" },
            { label: "QA",value:"qa" },
           
        ];

//jump to next status
export const JumpToNextStatus=(status)=>{
if(status=='Pending Approval'){
return 'Being Cooked'
}
else if(status=='Being Cooked'){
return 'Being Delivered'
}
else if(status=='Being Delivered'){
return 'Pending Payment'
}
else if(status=='Pending Payment'){
return 'Completed'
}
}

export  const actualStatus=[
     {label:'PENDING'},
    { label:'REJECTED'},
    { label:'APPROVED'}
 ]

 export const statusToCode=(status)=>{
     var val=0;
     if(status==="PENDING"){
     val=0;
     }
    if(status==="REJECTED"){
     val=2;
     }
    if(status==="APPROVED"){
     val=1;
     }

     return val;
 }