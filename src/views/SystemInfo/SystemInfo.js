import React from 'react';
import Button from "components/CustomButton/CustomButton.jsx";
import Checkbox from 'components/CustomCheckbox/CustomCheckbox';
import { Form, Formik } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import Card from "components/Card/Card.jsx";
import { ModuleTypeArray,ArrayType,RateType } from '../../components/Components/variables';
import {
    FormGroup,
    ControlLabel,
    FormControl,
    Grid,
    Row,
    Col

} from "react-bootstrap";

class SystemInformatoin extends React.Component {
   
    render() {
  
        //field validation using you
        const validationSchema = Yup.object().shape({
          DC_SystemSize: Yup.string().required(),  
          ModuleType:Yup.string().required(),
          ArrayType:Yup.string().required(),
          SystemLosses:Yup.string().required(),
          Tilt:Yup.string().required(),
          Azimuth:Yup.string().required(),
          RateType:Yup.string().required(),
           Rate:Yup.string().required(),
        });

        return (
          <Grid fluid >
<Card
            title="Inter System Info"
                            content={
          <div>

                    <Formik
                        initialValues={{
                            DC_SystemSize: '',
                            ModuleType:'',
                            ArrayType:'',
                            SystemLosses:'',
                            Tilt:'',
                            Azimuth:'',
                            RateType:'',
                            Rate:'',


                        }}
                        validationSchema={validationSchema}

                        onSubmit={values => {
                           
                            
                        }}
                        render={({
                            touched,
                            errors,
                            values,
                            handleChange,
                            setFieldValue
                          }) => (
                                <Form>
                                      <Row>
                                  <Col md={6}>
                                  <FormGroup validationState={
                                        touched.DC_SystemSize && errors.DC_SystemSize
                                            ? "error"
                                            : "success"
                                    }>
                                        <ControlLabel style={{ color: "black" }}>
                                        DC System Size (kW)
                                </ControlLabel>
                                        <FormControl
                                            placeholder={"D System Size"}
                                            name="DC_SystemSize"
                                            value={values.DC_SystemSize}
                                            onChange={
                                                handleChange
                                            }
                                        />
                                        {touched.DC_SystemSize && errors.DC_SystemSize && (
                                            <small className="text-danger">
                                                {errors.DC_SystemSize}
                                            </small>
                                        )}
                                    </FormGroup>
                                    </Col>


                                    <Col md={6}>
                                    <FormGroup
                                                    validationState={
                                                        touched.ModuleType && errors.ModuleType
                                                            ? "error"
                                                            : "success"
                                                    }
                                                >
                                                    <ControlLabel>
                                                    Module Type
                                                             </ControlLabel>
                                                    <Select
                                                        clearable={false}
                                                        placeholder="Single Select"
                                                        value={{ label: values.ModuleType }}
                                                        options={ModuleTypeArray}
                                                        onChange={value => {
                                                            if (value.label)
                                                                setFieldValue('ModuleType', value.label)
                                                        }}
                                                    />

                                                    {touched.ModuleType && errors.ModuleType && (
                                                        <small className="text-danger">
                                                            {errors.ModuleType}
                                                        </small>
                                                    )}
                                                </FormGroup>
                                    </Col>
                                    </Row>
                            
                            <Row>
                              <Col md={6}>
                              <FormGroup
                                                    validationState={
                                                        touched.ArrayType && errors.ArrayType
                                                            ? "error"
                                                            : "success"
                                                    }
                                                >
                                                    <ControlLabel>
                                                    Array Type
                                                             </ControlLabel>
                                                    <Select
                                                        clearable={false}
                                                        placeholder="Single Select"
                                                        value={{ label: values.ArrayType }}
                                                        options={ArrayType}
                                                        onChange={value => {
                                                            if (value.label)
                                                                setFieldValue('ArrayType', value.label)
                                                        }}
                                                    />

                                                    {touched.ArrayType && errors.ArrayType && (
                                                        <small className="text-danger">
                                                            {errors.ArrayType}
                                                        </small>
                                                    )}
                                                </FormGroup>
                              </Col>

                              <Col md={6}>
                              <FormGroup validationState={
                                        touched.SystemLosses && errors.SystemLosses
                                            ? "error"
                                            : "success"
                                    }>
                                        <ControlLabel style={{ color: "black" }}>
                                        System Losses (%)
                                </ControlLabel>
                                        <FormControl
                                            placeholder={"System Losses"}
                                            name="SystemLosses"
                                            value={values.SystemLosses}
                                            onChange={
                                                handleChange
                                            }
                                        />
                                        {touched.SystemLosses && errors.SystemLosses && (
                                            <small className="text-danger">
                                                {errors.SystemLosses}
                                            </small>
                                        )}
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row>
                              <Col md={6}>
                              <FormGroup validationState={
                                        touched.Tilt && errors.Tilt
                                            ? "error"
                                            : "success"
                                    }>
                                        <ControlLabel style={{ color: "black" }}>
                                        Tilt (deg):
                                </ControlLabel>
                                        <FormControl
                                            placeholder={"Tilt"}
                                            name="Tilt"
                                            value={values.Tilt}
                                            onChange={
                                                handleChange
                                            }
                                        />
                                        {touched.Tilt && errors.Tilt && (
                                            <small className="text-danger">
                                                {errors.Tilt}
                                            </small>
                                        )}
                                    </FormGroup>
                              </Col>


                              <Col md={6}>
                              <FormGroup validationState={
                                        touched.Azimuth && errors.Azimuth
                                            ? "error"
                                            : "success"
                                    }>
                                        <ControlLabel style={{ color: "black" }}>
                                        Azimuth (deg):
                                </ControlLabel>
                                        <FormControl
                                            placeholder={"Azimuth"}
                                            name="Azimuth"
                                            value={values.Azimuth}
                                            onChange={
                                                handleChange
                                            }
                                        />
                                        {touched.Azimuth && errors.Azimuth && (
                                            <small className="text-danger">
                                                {errors.Azimuth}
                                            </small>
                                        )}
                                    </FormGroup>
                              </Col>
                            </Row>
                            <Row>

                              <Col md={6}>
                              <FormGroup
                                                    validationState={
                                                        touched.RateType && errors.RateType
                                                            ? "error"
                                                            : "success"
                                                    }
                                                >
                                                    <ControlLabel>
                                                    Rate Type
                                                             </ControlLabel>
                                                    <Select
                                                        clearable={false}
                                                        placeholder="Single Select"
                                                        value={{ label: values.RateType }}
                                                        options={RateType}
                                                        onChange={value => {
                                                            if (value.label)
                                                                setFieldValue('RateType', value.label)
                                                        }}
                                                    />

                                                    {touched.RateType && errors.RateType && (
                                                        <small className="text-danger">
                                                            {errors.RateType}
                                                        </small>
                                                    )}
                                                </FormGroup>
                              </Col>

                              <Col md={6}>
                              <FormGroup validationState={
                                        touched.Rate && errors.Rate
                                            ? "error"
                                            : "success"
                                    }>
                                        <ControlLabel style={{ color: "black" }}>
                                        Azimuth (deg):
                                </ControlLabel>
                                        <FormControl
                                            placeholder={"Rate"}
                                            name="Rate"
                                            value={values.Rate}
                                            onChange={
                                                handleChange
                                            }
                                        />
                                        {touched.Rate && errors.Rate && (
                                            <small className="text-danger">
                                                {errors.Rate}
                                            </small>
                                        )}
                                    </FormGroup>
                                </Col>
                            </Row>
                                    <Button
                                     type="submit"
                                        bsStyle="info"
                                        fill
                                        wd
                                      
                                    >

                                      Result
                                    </Button>
                                </Form>
                            )}
                    />
 
                       </div>
                            }
                            />
                            </Grid>
        )
    }
}


export default SystemInformatoin