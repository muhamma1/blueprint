
import React from 'react';

import { formatDate } from '../../utils/helpers';
import { connect } from 'react-redux';
import { onSaveComment } from '../../store/actions/addSupportDocsComment';
import * as restaurantDetailAction from '../../store/actions/getRestaurantDetail';



class Board extends React.Component  {
  
   state={
       comments:[],
       inputComment:'',
       onCommentLoading:false
   }
    
   componentDidMount() {
       const {  docs } =this.props.restaurantDetails;
       this.setState({comments:docs.comments})
   }
   
  
  onSaveComment=()=>{
      this.setState({onCommentLoading:true})
     const data={
          comment:this.state.inputComment
      }
        this.setState({inputComment:''})
    this.props.onSaveCommentRequest(data,this.props.restaurantDetails._id).then(res=>{
       this.setState({onCommentLoading:false})
 this.props.onGetRestaurantDetail(this.props.restaurantDetails._id).then(resData=>{
       this.setState({comments:this.props.restaurantDetails.docs.comments})
 })
    }).catch(err=>{
 this.setState({onCommentLoading:false})
    })
    
  }
  
  
  render(){
      console.log("selected restaurant",this.props.restaurantDetails)
    return(
        <div>
      <div className="commentScroll">
        
        
        {this.state.comments.map((comment,key)=>(
        <div key={key} className="commentContainer">
            <div className="commentName">{comment.user.name}</div><br/>
           
        <div className="commentText">
            {comment.comment}
            </div>   
                <div className='commentDate'>{formatDate(comment.createdAt)}</div> 
      
      </div>
        ))}

        
      </div>
      <div className="shareCommentContainer">
          <textarea style={{border:'1px solid black'}} value={this.state.inputComment} onChange={(e)=>this.setState({inputComment:e.target.value})} placeholder="Write a comment.."></textarea> 
          <button disabled={this.state.onCommentLoading||this.state.inputComment===''} onClick={this.onSaveComment} className="btn btn-success">Save</button>
        </div>
      </div>
    );
  }
  
}


const mapDispatchToProp=dispatch=>{
    return{
         onSaveCommentRequest:(data,id)=>dispatch(onSaveComment(data,id)),
          onGetRestaurantDetail: (id) => dispatch(restaurantDetailAction.getSelectedRestaurant(id))
    }
}

export default connect(null,mapDispatchToProp)(Board);