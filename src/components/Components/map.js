import React from 'react';
import Select from "react-select";
import "react-select/dist/react-select.css";
import Button from "components/CustomButton/CustomButton.jsx";
import Card from "components/Card/Card.jsx";
import Map from '../GoogleMapAutoCompleat';
import {
  Col,
  Row,
  Grid
} from "react-bootstrap";


class MapA extends React.Component{

 
  
    state = {
      address: '',
      city: '',
      area: '',
      state: '',
      country: '',
      mapPosition: {
          lat: 0,
          lng: 0
      },
      markerPosition: {
          lat: 0,
          lng: 0
      },
      latInput: 0,
      lngInput: 0,
      nextScreen:false,
  }

  //on click to lnext button
  OnNext=()=>{
    this.setState({nextScreen:true})
  }
  onResponse = (res) => {

    this.setState({
        address: (res.LongAddress) ? res.LongAddress : '',
        area: (res.area) ? res.area : '',
        city: (res.city) ? res.city : '',
        state: (res.state) ? res.state : '',
        country: (res.country) ? res.country : '',
    })
}

onChangePosition = (latValue, lngValue) => {
  this.setState({

      markerPosition: {
          lat: latValue,
          lng: lngValue
      },
      mapPosition: {
          lat: latValue,
          lng: lngValue
      },
      latInput: latValue,
      lngInput: lngValue
  })
}
render(){
  const listOfCountry = [];
  const selectedCountry = { label: this.state.country }
const listOfCity=[]
  const selectedCity = { label: this.state.city }
    return(

      <div>
 <Card
                            //title="Address Details"
                            content={
                                <div>
                                    <h4 className='title_bar'>Address Details</h4>

                                    <Map
                                        center={{ lat: this.state.mapPosition.lat, lng: this.state.mapPosition.lng }}
                                        height='300px'
                                        zoom={15}
                                        address={this.state.address}
                                        onResponse={this.onResponse}
                                        mapPosition={this.state.mapPosition}
                                        markerPosition={this.state.markerPosition}
                                        onChangePosition={this.onChangePosition}
                                        //location={location}
                                    />
                                    <div style={{ marginTop: '50px' }} >
                                        <Row>
                                            <Col md={6}>
                                                <div className="form-group">
                                                    <label htmlFor="Latitude">Latitude</label>
                                                    <input type="number" name="latitude" className="form-control" onChange={(e) => {
                                                        this.setState({

                                                            latInput: e.target.value
                                                        })
                                                    }} value={this.state.latInput} />
                                                </div>
                                            </Col>
                                            <Col md={6}>
                                                <div className="form-group">
                                                    <label htmlFor="Longitude">Longitude</label>
                                                    <input type="number" name="longitude" className="form-control" onChange={(e) => {
                                                        this.setState({

                                                            lngInput: e.target.value
                                                        })
                                                    }} value={this.state.lngInput} />
                                                </div>
                                            </Col>
                                        </Row>

                                    </div>
                                    <Row>

                                      <Col md={6}>
                                      <div className="form-group">
                                        <label htmlFor="">City</label>
                                        <Select
                                            clearable={false}
                                            placeholder="Single Select"
                                            value={selectedCity}
                                            options={listOfCity}
                                            onChange={value => {
                                                this.setState({ city: value.label })
                                            }}
                                        />
                                    </div>
                                    </Col>
                                    <Col md={6}>
                                    <div className="form-group">
                                        <label htmlFor="">Area</label>
                                        <input type="text" name="area" className="form-control" onChange={this.onChange} value={this.state.area} />
                                    </div>

                                      </Col>
                                    </Row>
                                    
                                    <Row>
                                      <Col md={6}>
                                      <div className="form-group">
                                        <label htmlFor="">Country</label>
                                        <Select
                                            clearable={false}
                                            placeholder="Single Select"
                                            value={selectedCountry}
                                            options={listOfCountry}
                                            onChange={value => {
                                                this.setState({ country: value.label })
                                            }}
                                        />
                                    </div>
                                   </Col>
                                    <Col md={6}>
                                    <div className="form-group">
                                        <label htmlFor="">Address</label>
                                        <input type="text" name="address" className="form-control" onChange={this.onChange} value={this.state.address} />
                                    </div>
                                      </Col>
                                    </Row>

                                    
                                    <Button
                                        disabled={this.state.editAddressLoading}
                                        type="Next"
                                        bsStyle="info"
                                        fill
                                        wd
                                        onClick={() => {
                                            this.OnNext()
                                        }}
                                    >
                                        {this.state.editAddressLoading ? 'Loading...' : 'Next'}
                                        <i className={this.state.editAddressLoading ?
                                            "fa fa-spin fa-spinner" : ''} />


                                    </Button>

                                </div>

                            } />

                            {
                              this.state.nextScreen?
                              this.props.history.push('/systemInformation')
                             :null
                            }
      </div>
    )
  }
}

export default MapA;