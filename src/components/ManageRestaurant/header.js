import React from 'react';

import { Row,Col } from 'react-bootstrap';
//styling
const style = {
  countSpin: 'rgb(251, 64, 75)',
}
const header =(props)=>{
    const {approved,mywork,pending,rejected,restaurantrequest}=props.documentsCount;
    const {selectedTab} =props;
    return(
        <div>
 <Row className="restaurant_header" >
          <Col md={2}>
            <br />
            <label>Restaurants:</label>
          </Col>
          <Col onClick={
            () => {
               props.onClick('pending');
            }
          } className={selectedTab === 'pending' ? "selected" : "options"} md={2}>
            <i className="fa fa-spinner" />
            <br />
            PENDING <span className="badge badge-danger badge-pill" style={{ backgroundColor: style.countSpin }} >{pending?pending:'0'}</span>
          </Col>
          <Col onClick={
            () => {
              props.onClick('rejected');
            }
          } className={selectedTab === 'rejected' ? "selected" : "options"} md={2}>
            <i className="fa fa-times-circle" />
            <br />
            REJECTED <span className="badge badge-danger badge-pill" style={{ backgroundColor: style.countSpin }} >{rejected?rejected:'0'}</span>
          </Col>
          <Col onClick={
            () => {
               props.onClick('approved');
            }
          } className={selectedTab === 'approved' ? "selected" : "options"} md={2}>
            <i className="fa fa-check" />
            <br />
            APPROVED <span className="badge badge-danger badge-pill" style={{ backgroundColor: style.countSpin }} >{approved?approved:'0'}</span>
          </Col>
          <Col onClick={
            () => {
               props.onClick('myWork')
            }
          } className={selectedTab === 'myWork' ? "selected" : "options"} md={2}>
            <i className="fa fa-magnet" />
            <br />
            MY WORK <span className="badge badge-danger badge-pill" style={{ backgroundColor: style.countSpin }} >{mywork?mywork:'0'}</span>
          </Col>
          <Col onClick={
            () => {
             props.onClick('addRequests')
            }
          } className={selectedTab === 'addRequests' ? "selected" : "options"} md={2}>
            <i className="fa fa-plus" />
            <br />
            ADD REQUESTS <span className="badge badge-danger badge-pill" style={{ backgroundColor: style.countSpin }} >{restaurantrequest?restaurantrequest:'0'}</span>
          </Col>

        </Row>
        </div>
    )
}

export default header;