

import Dashboard from '../views/Dashboard/Dashboard';
import SystemInformation from '../views/SystemInfo/SystemInfo';
var dashboardRoutes = [

  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard
  },
  {
    path: "/systemInformation",
    name: "System Information",
    icon: "pe-7s-graph",
    component: SystemInformation
  },


  
    { redirect: true, path: "/", pathTo: "/dashboard", name: "Dashboard" }
];
export default dashboardRoutes;
