
export const appTitle = "Blueprint Power"
export const brand = "Blueprint"
export const getTabTitle = (tab) => {
    return `${(tab ? tab + ' | ' : '')} ${appTitle}`;
}
export const OrderStatus = {
    Pending: "Pending",
    PendingApproval: "Pending Approval",
    BeingCooked: "Being Cooked",
    BeingDelivered: "Being Delivered",
    PendingPayment: "Pending Payment",
    Cancelled: "Cancelled",
    Completed: "Completed",

}

export const paymentMethodEnum = {
    'online': 'Online',
    'cod': 'Cash On Delivery',
    'redeem': 'Redeem'
}
export const daysCount=[
    {label:'Monday'},
    {label:'Tuesday'},
    {label:'Wednesday'},
    {label:'Thursday'},
    {label:'Friday'},
    {label:'Saturday'},
    {label:'Sunday'},
]

export const timeCount=[
    {label:'1:00am'},
    {label:'1:30am'},
    {label:'2:00am'},
    {label:'2:30am'},
    {label:'3:00am'},
    {label:'3:30am'},
    {label:'4:00am'},
    {label:'4:30am'},
    {label:'5:00am'},
    {label:'5:30am'},
    {label:'6:00am'},
    {label:'6:30am'},
    {label:'7:00am'},
    {label:'7:30am'},
    {label:'8:00am'},
    {label:'8:30am'},
    {label:'9:00am'},
    {label:'9:30am'},
    {label:'10:00am'},
    {label:'10:30am'},
    {label:'11:00am'},
    {label:'11:30am'},
    {label:'12:00pm'},
    {label:'12:30pm'},
    {label:'1:00pm'},
    {label:'1:30pm'},
    {label:'2:00pm'},
    {label:'2:30pm'},
    {label:'3:00pm'},
    {label:'3:30pm'},
    {label:'4:00pm'},
    {label:'4:30pm'},
    {label:'5:00pm'},
    {label:'5:30pm'},
    {label:'6:00pm'},
    {label:'6:30pm'},
    {label:'7:00pm'},
    {label:'7:30pm'},
    {label:'8:00pm'},
    {label:'8:30pm'},
    {label:'9:00pm'},
    {label:'9:30pm'},
    {label:'10:00pm'},
    {label:'10:30pm'},
    {label:'11:00pm'},
    {label:'11:30pm'},
    {label:'12:00am'},
    {label:'12:30am'},    
]

export const enumBudget = (selected) => {

    let b = {
        "Low": false,
        "Average": false,
        "High": false,
        "Elite": false
    }
    if (selected) { b[selected] = true; }
    return b;
}

export const delivers = (selected) => {

    let b = {
        "Yes": false,
        "No": false,
    }
    if (selected) { b[selected] = true; }
    return b;
}

export const enumReviewStatus = {
    'Published': 'Published',
    'Rejected': 'Rejected',
    'Reported': 'Reported',
    'Deleted': 'Deleted'
}
// export const getReviewRatingByType = (rating, type) => { //type= food, restaurant

//     let ratingObj = { overall: rating['overall'] };

//     if (type.toLowerCase() == "restaurant") {
//         ratingObj.service = rating["service"]
//         ratingObj.ambience = rating["ambience"]

//     }
//     else {
//         ratingObj.hygiene = rating["hygiene"]
//         ratingObj.taste = rating["taste"]
//     }
//     return ratingObj

// }

export const time =
    [
        { value: 5, label: "05" },
        { value: 10, label: "10" },
        { value: 20, label: "20" },
        { value: 25, label: "25" },
        { value: 30, label: "30" },
        { value: 35, label: "35" },
        { value: 40, label: "40" },
        { value: 45, label: "45" },
        { value: 50, label: "50" },
        { value: 55, label: "55" },
        { value: 60, label: "60" },
    ]

export const pageSize = 10;
export const ReviewsPageSize = 10;
export const maxRating = "5.0";
export const messages = {
    pendingApprovalSuccess: "Order approved successfully",
    beingCookedApproveSuccess: "Order being cooked successfully",
    beingDeliveredApproveSuccess: "Order being delivered successfully",
    pendingPaymentApproveSuccess: "Pending payment approved successfully",
    orderCancelledSuccess: "Order cancelled successfully",
    OrderUpdateFail: "Something went wrong"
}
export const siteInfo = {
    email: "care@fikifoo.com"
}


