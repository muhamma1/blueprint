const baseUrl = process.env.REACT_APP_API_URL;

const fikifooApi = {
    login: baseUrl + '/api/login', //'/api/login'
    GetRestaurantByID: (restaurantID) => {
        return baseUrl + `/api/rp-getRestaurantByID/${restaurantID}`
    },


////////////////category//////////////// 
////0 = pending, 1 = Approved, 2 = Rejected
//////////////////////////////////////////
//get restaurants 
 getRestaurantsByCategory: (params) => {
        return baseUrl + `/api/up-getRestaurantsByCategory?page=${params.page}&limit=${params.limit}&category=${params.category}&title=${params.title}&city=${params.city?params.city:''}`
    },

//get restaurant request.
     getRequest: (params) => {
        return baseUrl + `/api/up-restaurantRequests?page=${params.page}&limit=${params.limit}&title=${params.title}`
    },

    //get manage restaurant count
     manageRestaurantCount: () => {
        return baseUrl + `/api/up-manageRestaurantsCount`
    },

    //get restaurant reviews
     restaurantReviews: (id,page,status) => {
        return baseUrl + `/api/rp-getAllReviews/${status}/${id}/${page}?`
    },

    //get user management
     anOrDisAdmin: (userId,type,roleId,active) => {
         if(active){
        return baseUrl + `/api/disableAdminUser/${userId}/${type}/${roleId}`
         }else{
        return baseUrl + `/api/enableAdminUser/${userId}/Admin/${roleId}`
         }
    },

    //anAble or disable admin
     getUserManagement: (params) => {
        return baseUrl + `/api/searchAllAdmins/null/`
    },

    //get restaurant request.
     getMyWork: (params) => {
        return baseUrl + `/api/up-getMyWork?page=${params.page}&limit=${params.limit}&title=${params.title}`
    },

      //edit commission
     editCommission: (id,percent) => {
        return baseUrl + `/api/updateCommission/${id}?percentage=${percent}`
    },

    //get referral code
     getReferral: (id) => {
        return baseUrl + `/api/up-getRestaurantReferralCode/${id}`
    },

   //get statues
     getStatuses: (params) => {
        return baseUrl + `/api/statuses`
    },
     //get roles
     getRoles: () => {
        return baseUrl + `/api/roles`
    },


    //get all orders
     getOrders: (params) => {
        return baseUrl + `/api/up-getAllCarts/${params.type}?page=${params.page}&limit=${params.limit}&title=${params.title}&from=${params.from}&to=${params.to}`
    },


//get all restaurant
 getAllRestaurant: (params) => {
        return baseUrl + `/api/up-getAllRestaurants?page=${params.page}&limit=${params.limit}&title=${params.title}`
    },

 //get all franchiese 
 getfranchises: () => {
        return baseUrl + `/api/branches`
    },
//get restaurant details
 getRestaurantDetail: (id) => {
        return baseUrl + `/api/up-getRestaurantByID/${id}`
    },

    //get delivery personnel
 getDeliveryPersonnel: (params) => {
        return baseUrl + `/api/getAllDeliveryPersons/${params.id}`
    },

    
    //get restaurant admin users
 getRestaurantAdmins: (params) => {
        return baseUrl + `/api/searchAllAdmins/${params.page}/${params.id}`
    },

       //get verified restaurants
 getVerifiedRestaurant: (params) => {
        return baseUrl + `/api/getVerifiedRestaurants/${params.limit}/${params.page}`
    },


    //get restaurant statuses
 getRestaurantStatuses: () => {
        return baseUrl + `/api/restaurantStatuses`
    },
//post food image
 postFoodImage: () => {
        return baseUrl + `/api/saveDeliveryPersonImages`
    },
    //adding new food
 addFood: (id) => {
        return baseUrl + `/api/up-addFood/${id}`
    },
      //adding new category
 addCategory: (id) => {
        return baseUrl + `/api/up-addCategory/${id}`
    },

    //changing status
 changeStatus: (id) => {
        return baseUrl + `/api/changeOrderStatus/${id}`
    },

    //register new admin
 registerAdmin: () => {
        return baseUrl + `/api/registerAdmin`
    },
    //add new role
 addRole: () => {
        return baseUrl + `/api/role`
    },

    //add support Docs images 
 addSupportDocImage: (id) => {
        return baseUrl + `/api/up-addimage/${id}`
    },

     //set referral code
 setReferralCode: (code) => {
        return baseUrl + `/api/sendReferralId/${code}`
    },

    
    //add support Docs comment 
 addSupportDocComment: (id) => {
        return baseUrl + `/api/up-addcomment/${id}`
    },
    //edit role
 editRole: (id) => {
        return baseUrl + `/api/role/${id}`
    },

    //add Status
 addStatus: () => {
        return baseUrl + `/api/restaurantStatus`
    },

    //register new restaurant
 addNewRestaurant: () => {
        return baseUrl + `/api/up-addRestaurant`
    },

    //add delivery personnel
 addDeliveryPerson: () => {
        return baseUrl + `/api/saveDeliveryPerson`
    },

    
    //add register restaurant user 
 registerRestaurantUser: () => {
        return baseUrl + `/api/registerAdmin`
    },

    //update delivery personnel
 updateDeliveryPerson: () => {
        return baseUrl + `/api/updateDeliveryPerson`
    },

    //update restaurant user
 updateRestaurantUser: () => {
        return baseUrl + `/api/updateRestaurantAdmin`
    },

    //update Status
 updateStatus: () => {
        return baseUrl + `/api/updateRestaurantStatus`
    },

    //changing status
 updateToNextStatus: () => {
        return baseUrl + `/api/updateRestaurants`
    },

      //edit suers
 editUsers: () => {
        return baseUrl + `/api/updateAdminUser`
    },
 //add on 
 addOn: (id) => {
        return baseUrl + `/api/up-addAddon/${id}`
    },
    //update restaurant food
 updateRestaurantFood: (id) => {
        return baseUrl + `/api/up-editFood/${id}`
    },
//update add on
updatingAddOn: (id) => {
        return baseUrl + `/api/up-editAddon/${id}`
    },
    //updating food category
updatingFoodCategory: (id) => {
        return baseUrl + `/api/up-editCategory/${id}`
    },

    //edit contact detial
editContactDetail: (id) => {
        return baseUrl + `/api/up-updateContactDetails/${id}`
    },

    //edit characteristic detial
editCharacteristicDetail: (id) => {
        return baseUrl + `/api/up-updateCharacteristicsDetails/${id}`
    },

       //edit delivery detial
editDeliveryDetail: (id) => {
        return baseUrl + `/api/up-updateDeliveryDetails/${id}`
    },

       //edit cover iamges
editCoverImg: (id) => {
        return baseUrl + `/api/up-updateCoverImage/${id}`
    },
    //edit Logo
editLogo: (id) => {
        return baseUrl + `/api/up-updateLogo/${id}`
    },
       //edit Ambience iamges
editAmbienceImg: (id) => {
        return baseUrl + `/api/up-updateAmbienceImage/${id}`
    },


    
       //edit other detial
editOtherDetail: (id) => {
        return baseUrl + `/api/up-updatePrinterDetails/${id}`
    },
           //edit working hours
editWorkingHours: (id) => {
        return baseUrl + `/api/up-updateHours/${id}`
    },

     //edit address detail
editAddressDetail: (id) => {
        return baseUrl + `/api/up-updateAddressDetails/${id}`
    },

//updating basic detail
editBasicDetail: (id) => {
        return baseUrl + `/api/up-updateBasicDetails/${id}`
    },

    //sorting categories
sortingCategories: (id) => {
        return baseUrl + `/api/up-customizeCategories/${id}`
    },

        //sorting foods
sortingFoods: (id) => {
        return baseUrl + `/api/up-customizeFoods/${id}`
    },

//delete addOn
deleteAddOn:(foodId,addOnId)=>{
    return baseUrl+`/api/up-deleteAddon?foodId=${foodId}&addonId=${addOnId}`
},

//delete category
deleteRole:(id)=>{
    return baseUrl+`/api/deactivateRole/${id}`
},

//delete delivery persons
deleteDeliveryPerson:(id)=>{
    return baseUrl+`/api/deactivateDeliveryPerson/${id}`
},

//delete roles
deleteCategory:(id)=>{
    return baseUrl+`/api/up-deleteCategory/${id}`
},

//deleting food
deleteFood:(id)=>{
    return baseUrl+`/api/up-deleteFood/${id}`
},

//deleting restourent
deactivateRestaurant:(id)=>{
    return baseUrl+`/api/deactivateRestaurant/${id}`
},

    GetOrders: (restaurantID, status, page = 1, from, to, username, orderNumber) => {
        return baseUrl + `/api/rp-getAllCarts/${status}/${restaurantID}/${page}?from=${from}&&to=${to}&&username=${username}&&orderNumber=${orderNumber}`
    },
    UpdateOrderStatus: (orderId) => {
        return baseUrl + `/api/changeOrderStatus/${orderId}`
    },
    updateCart: (id) => {
        return baseUrl + `/api/updateCart/${id}`
    },
    CancelOrder: (orderId) => {
        return baseUrl + `/api/deactivateOrder/${orderId}`
    },
    GetDeliveryPersonsbyRestaurantID: (restaurantID) => {
        return baseUrl + `/api/getAllDeliveryPersons/${restaurantID}`
    },
    GetReviews: (restaurantID, status, page) => {
        return baseUrl + `/api/rp-getAllReviews/${status}/${restaurantID}/${page}?`
    },
    AddDeliveryPerson: () => {
        return baseUrl + `/api/saveDeliveryPerson`
    },
    SaveDeliveryPersonImage: () => {
        return baseUrl + `/api/saveDeliveryPersonImages`
    },
    DeleteDeliveryPerson: (id) => {
        return baseUrl + `/api//deactivateDeliveryPerson/${id}`
    },
    UpdateDeliveryPerson: () => {
        return baseUrl + `/api/updateDeliveryPerson`
    },
    GetReports: (restaurantID, page = 1, from, to, username, orderType) => {
        return baseUrl + `/api/reportsOfCart/${restaurantID}/${page}?from=${from}&&to=${to}&&name=${username}&&orderType=${orderType}`
    }

}

export default fikifooApi;